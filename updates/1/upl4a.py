import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import os

class App(QDialog):

    def __init__(self):
        super().__init__()
        self.title = 'L4Update'
        self.left = 10
        self.top = 10
        self.width = 510
        self.height = 100
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self.title)
        #self.setGeometry(self.left, self.top, self.width, self.height)
        self.setFixedSize(self.width, self.height) 
        self.createHorizontalLayout()
        
        windowLayout = QVBoxLayout()
        windowLayout.addWidget(self.centerGroupBox)
        self.setLayout(windowLayout)
        
        self.show()
    
    def createHorizontalLayout(self):
        self.centerGroupBox = QGroupBox("You are up to date")
        layout = QHBoxLayout()
         
        v64 = QPushButton("Exit", self)
        v64.clicked.connect(self.button1)
        layout.addWidget(v64)


        
        self.centerGroupBox.setLayout(layout)
    
     
    @pyqtSlot()
    def button1(self):
        sys.exit()
    
    
    




if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())